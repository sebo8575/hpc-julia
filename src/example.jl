using Dates

cd(ENV["WORK"])
if !ispath("testingdir")
    mkdir("testingdir")
end
cd("testingdir")

outname = "OUTPUT_" * Dates.format(now(), "yyyy-mm-dd") * ".txt"

println("About to open " * outname)
io = open(outname, "a")
println(io, Dates.format(now(), "yyyy-mm-ddTHH:MM:SS" ) * " FROM " * gethostname())
close(io)
println("Written to " * outname)
println("in \$WORK/testingdir - - - hopefully ;)")
println(stderr, "This program is still boring")
