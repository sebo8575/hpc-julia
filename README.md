# HPC-Julia

Small sample Project showing the use of Julia on HPC, for a more detailed (and up to date) description please visit:

https://wiki.hpcuser.uni-oldenburg.de

or the newer (but unter heavy (re)contruction)

https://hpcwiki.uol.de

## Accessing the cluster

This is independend of the used programming language.

#### Very basic way and some notes:

```
ssh ABCD1234@rosa.hpc.uni-oldenburg.de
```

CAVEAT: This only works from a uni-oldenburg IP, so you either need to connect from a computer inside the university or connect to universities network via VPN!

Actually there is not ONE host rosa. This DNS name 'rosa' points to 4 loginnodes `hpcl001` - `hpcl004` you are connected to in an load balanced order, but all folders are mounted remote so they are the same on all these nodes. This just matters if you are running some longer running process (for example a download) on one node in screen or tmux and after reconnect you are not seeing this thing again - in those cases you have to note down the actual loginnode (for example `hpcl102.hpc.uni-oldenburg.de`) - yes it ist 102, not 002 ...

IMPORTANT: Never do huge calculations on these login nodes, compiling, downloading etc is ok, but otherwise use srun, sbatch etc...

#### a bit smarter

Modify on your (linux-) machine your `~/.ssh/config`:

```
Host rosa
    user abcd1234
    HostName rosa.hpc.uni-oldenburg.de
```

Now you have defined an alias for `rosa.hpc....` and also told your ssh client to use `abcd1234` as user name, so instead of the above mentioned command you can just do a

```
ssh rosa
```

If you also copy your public key with `ssh-copy-id rosa` you will never need a password again. Eventually generate a new public/private key with `ssh-keygen -t ecdsa`, this creates two files in `.ssh` named `id_ecdsa` and `id_ecdsa.pub`. With the command `ssh-copy-id` the content of the `.pub` file is appended to the file `.ssh/authorized_keys` on the remote host.

Best of all, this works also for `scp` and `rsync`, so you can do for example

```
rsync -arv MY_PROJECT_FOLDER rosa:projects/
```

the folder `projects`  has to exist before, if you do not append the / to  MY_PROJECT_FOLDER a folder with the same name is created (or synced) on rosa, with the / appended only the content of the folder is copied/synced ...

Another nice thing is mount via ssh - for eample to mount your $WORK from rosa you can create a folder `~/HPCWORK` (name is totally up to you) and 

```
sshfs rosa:/gss/work/ABCD1234 ~/HPCWORK
ls ~/HPCWORK
```


## Storage on the HPC

You have mainly 3 "drives" **TODO: bit out of data**

| Name | Remarks |
| --- | --- |
| $HOME | Backed up, versioned rather small accessed via NFS (max 10Gbit), limited, configurations, programs, ... |
| $DATA | Backed up, versioned rather big (10TB) accessed via NFS (max 10Gbit), also slow, storage of stuff used on a long term basis |
| $WORK | Connected via infiniband, mostly 4-10 times faster then NFS so prefered use for large or many files ... |

Some more for special use, see also https://wiki.hpcuser.uni-oldenburg.de/index.php?title=File_system_and_Data_Management

On $HOME and $DATA you also have snapshots allowing one to access older versions of files - see for example `ls ./snapshots` on those drives

## Using Julia on the commandline

Programs on the cluster are organized in modules, so after logging in to carl the command `julia` will reply  not found!

Generally you do a `module spider julia` to find the options. Reading the response you learn several things:

- The module is named Julia and it is available in various versions like  1.0.1, 1.0.5 and 1.6.1, ....
- the full name is `Julia/1.9.2` or `Julia/1.6.1-linux-x86_64`
- If you look at the detail page for one version `module spider Julia/1.9.2` you see for example that the module depends on `hpc-env/13.1`

So to work with julia you have to run (currently) the following commands:

```
module load hpc-env/13.1
module load Julia/1.9.2
```

one can also use Julia without the version number, but the default version can change from time to time.

Now the command `julia` works. To run the example from this repo just enter `julia src/example.jl` which will create an examplefile in `$WORK/testingdir`

## Sending the command to the cluster

As mentioned one should never do heavy work on the login nodes -- other user will get angry ;)

To send your program to the cluster is done through a queing system called slurm. You have to describe requirements for your job and slurm will find the best node for you. These options are specified in a slurm file, a very simple one looks like this:

```#!/bin/bash
#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --mem=2G
#SBATCH --time=0-0:02
#SBATCH --output=example1.1-R.%j.out
#SBATCH --error=example1.1-R.%j.err
#SBATCH --mail-type=START,END,FAIL
#SBATCH --mail-user=matthias.schroeder@uol.de
#SBATCH --partition=rosa.p

module load hpc-env/13.1
module load Julia/1.9.2
julia src/example.jl
echo "finished at `date`"
```

this file is saved as `example.sbatch`, roughly this a batch file, you can use any bash functionality you want but it is not to be run as such but instead it is started via `sbatch`

interesting part is the beginning comments:

How many nodes, tasks per node (equals number of cores to use, see below), expected memory usage, expected runtimer etc. This is upper limit, if our process will use more than 2G ram it will be killed, if it takes longer than 2 minutes it will be killed and so on.

output and error define where you can find the programs output to stderr and stdout, mail-user is the address which will receive mail on process end.

partition is to pre-select the available computers, in case you have very high memory requirements you choose mpcb (b for big) see: https://wiki.hpcuser.uni-oldenburg.de/index.php?title=Partitions

More details can be found on https://wiki.hpcuser.uni-oldenburg.de/index.php?title=SLURM_Job_Management_(Queueing)_System#Information_on_sbatch-options

after starting via

```
sbatch example.sbatch
```

you see just a

```
Submitted batch job 23870435
```

which means your job is submitted whether it is active or done can be checked via one of the commands

```
squeue -u abcd1234  # all jobs submitted by you not done for now
sstat -j 23870435   # detail;ed info for job id ...
sacct               # your last jobs (finished and running too)
```

Helpful can be a command to cancel a wrong process: just do a

```
scancel 23870435
```

## Extending maximum runtime from 7 days up

Most queue have a time limit of 7 days but you can still start longer running processes.
You have to select a special qos.
So one example can be:

```sh
sacctmgr show qos
sbatch --partition mpcs.p --qos long_mpcs.q --time 21-0 jobscript.sh
```

First command shows available qos, second command actually starts the process, all parameters can also be set in the slurm file `jobscript.sh`  
Please keep some things in mind:
-  downtimes scheduled or by surprise can interfere)
- Those longer running processes are limited in the number of cores/cpus to avoid blocking the cluster for a long time.

## how many cores, nodes, .... memory, ...

You have at least three configuration parameters `--nodes, --ntasks, --cpus-per-task` which could cause a bit confusion lets try to get it sorted out:

`--cpus-per-task` the most handy parameter: You want to use 16 cores on one computer: just set `--cpus-per-task=16`, check before that in your partition there are enough hosts with that many cores!  
`--nodes` the maximum amount of hosts to use - these are actual computers so often it does not make sense to dirstribute one process over multiple nodes unless you program is capable of handling that (keyword *mpi* for example) 
`--ntasks` how many cores to use in total, so `--nodes=1` and `--ntasks=16` is effectively the same as `--cpus-per-task=16`

For memory just do an estimation and request '*a bit more*', also make sure that inb your partition are nodes with enough mem.

And runtime - well just estimate - do some small tests and try to extrapolate.

If either mem or runtime exceeds the give parameter your program will be killed by the queuing system...
